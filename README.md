# Noisy Computation's English language locale for Sweden.

This package provides a Glibc/POSIX locale whose LC_TIME matches the Qt/KDE en_SE profile.

# Background

Glibc/POSIX and Qt/KDE (more broadly Unicode CLDR/ICU) have diverged and
efforts to make Glibc/POSIX comply with CLDR have apparently stalled after
being announced in 2016.

Glibc/POSIX has en_DK with an acceptable ISO 8601 compliant LC_TIME. But
Qt/KDE's version of en_DK has a different LC_TIME. Incidentally Qt/KDE's
version is inconsistent with CLDR, but KDE's developers have flagged this as
not KDE's problem and it won't be fixed. Fixing en_DK in Qt requires compiling
much of Qt core and is not acceptable.

Qt/KDE has profile en_SE with an acceptable ISO 8601 compliant LC_TIME.
Glibc/POSIX doesn't have this profile at all, but it is very easy to add
Glibc/POSIX locales without compiling anything. Indeed the Glibc locale system
is intended to support this sort of runtime modification.

# Details

The profile is copied from en_US with the exception of LC_TIME. As such, it
deviates from other unofficial profiles that seek to merge en_US and ISO 8601.

# Instructions

Run `makepkg -i` in the repo root directory. If you wish, you can cun `makepkg`
and install the package manually with `pacman -Su locale-en_se...`.

After installing the package:

  1. Run 'locale-gen' as root.
  2. Add the following to /etc/locale.conf:
```
       LC_CTYPE=en_US.UTF-8
       LC_TIME=en_SE.UTF-8
```
Note that specifying LC_TIME is optional in KDE. Instead, you can choose en_SE
in KDE regional settings for Time.

# Manual "Installation"

It is advisable to make and install the package, because that way it will be
under the management of `pacman`. Remember that my primary system management
rule is that files may only be placed into the `/usr` tree if they are part
of a package and have a PKGBUILD file. (Yes, even `/usr/local`.).

But if you insist, you can do the following:

  1. Drop `en_SE` into /usr/share/i18n/locales.
  2. Run 'locale-gen' as root.
  3. Add the following to /etc/locale.conf:
```
       LC_CTYPE=en_US.UTF-8
       LC_TIME=en_SE.UTF-8
```
Note that specifying LC_TIME is optional in KDE. Instead, you can choose en_SE
in KDE regional settings for Time.

